from copy import deepcopy
from itertools import izip
from modules.repeat import Repeat


class RepeatFinder():

    def __init__(self, min_period, max_period, min_repeat_kmer):
        self.min_period = min_period
        self.max_period = max_period
        self.min_repeat_kmer = min_repeat_kmer

    @staticmethod
    def _periods(locs):
        return [x[0]-y[0] for x, y in izip(locs[1:], locs[:-1])]

    def _build_repeat(self, periods, pos):
        repeat = Repeat(pos)

        temp_period = 0

        for idx in xrange(pos, len(periods)):
            temp_period += periods[idx]

            if temp_period > self.max_period:
                return repeat

            elif temp_period >= self.min_period:
                repeat.extend(idx, temp_period)
                temp_period = 0

        else:
            return repeat

    def find_repeats(self, candidate):
        periods = self._periods(candidate.locs)

        if not periods:
            return

        idx = 0

        while idx < len(periods):
            repeat = self._build_repeat(periods, idx)

            if repeat.length >= self.min_repeat_kmer:
                candidate.add_repeat(deepcopy(repeat))

            idx += repeat.length
