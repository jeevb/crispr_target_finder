class Candidate():

    def __init__(self, source=None, source_len=None, pattern=None, word=None):
        self.source = source
        self.source_len = source_len
        self.pattern = pattern
        self.word = word

        self.locs = []

        self.repeats = []
        self.extras = []

    def add_loc(self, loc):
        self.locs.append(loc)

    def sort_locs(self):
        self.locs.sort()

    def add_repeat(self, repeat):
        repeat.start = self.locs[repeat.start][0]
        repeat.end = self.locs[repeat.end][1]

        self.repeats.append(repeat)

    def add_extras(self, source, repeats):
        for repeat in repeats:
            self.extras.append("%s:%i-%i(%i)" % (
                source,
                repeat.start,
                repeat.end,
                repeat.length
            ))

    @property
    def occurrences(self):
        return len(self.locs)

    def __str__(self):
        return self.pattern["format"].format(word=self.word)

    def __len__(self):
        return len(str(self))
