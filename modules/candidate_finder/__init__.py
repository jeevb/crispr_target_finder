from Bio import SeqIO
from modules.binding_simulator import BindingSimulator
from modules.candidate import Candidate
from modules.cross_checker import CrossChecker
from modules.repeat_finder import RepeatFinder
from modules.word_miner import WordMiner


class CandidateFinder():

    def __init__(self, config):
        self.config = config

        self.rf = RepeatFinder(
                self.config.min_period,
                self.config.max_period,
                self.config.min_repeat_kmer
            )

        self.xref = CrossChecker(
            self.config.subject_fasta,
            self.config.patterns,
            self.config.xref_min_period,
            self.config.xref_max_period,
            self.config.xref_max_repeat_kmer
        )

        self.sim = BindingSimulator(
            self.config.min_footprint,
            self.config.trials
        )

    def _find_candidates(self, pattern):
        wm = WordMiner(
            pattern["word_miner"],
            self.config.min_word_length,
            self.config.max_word_length
        )

        for record in SeqIO.parse(open(self.config.subject_fasta, "rU"), "fasta"):
            candidates = dict()
            sequence = str(record.seq).lower()
            sequence_len = len(sequence)

            for start, end, word in wm.find_words(sequence):
                try:
                    candidates[word]
                except KeyError:
                    candidates[word] = Candidate(record.name, sequence_len, pattern, word)

                candidates[word].add_loc((start, end))

            for word, candidate in candidates.iteritems():
                if candidate.occurrences < self.config.min_repeat_kmer:
                    continue

                candidate.sort_locs()

                self.rf.find_repeats(candidate)

                if not candidate.repeats:
                    continue

                self.xref.find_extra_repeats(candidate)
                self.sim.measure_binding_potential(sequence, candidate)

                yield candidate

    def candidates(self):
        for pattern in self.config.patterns:
            for candidate in self._find_candidates(pattern):
                yield candidate
