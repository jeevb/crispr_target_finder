import random
import re

from collections import defaultdict
from operator import itemgetter


class BindingSimulator():

    def __init__(self, min_footprint, trials):
        self.min_footprint = min_footprint
        self.trials = trials

    @staticmethod
    def set_mask(sequence, start, end):
        return "%s%s%s" % (
            sequence[:start],
            "x" * (end-start),
            sequence[end:]
        )

    def measure_binding_potential(self, sequence, candidate):
        tail_len = self.min_footprint-len(candidate) if self.min_footprint > len(candidate) else 0

        for repeat in candidate.repeats:
            region = sequence[repeat.start-tail_len:repeat.end+tail_len]

            repeat.binding_potential = self._binding_distribution(
                region,
                re.compile(candidate.pattern["simulator"].format(
                    word=candidate.word,
                    tail_len=tail_len
                ))
            )

    def _binding_distribution(self, region, pattern):
        density_dist = defaultdict(int)

        for i in xrange(self.trials):
            bind_count = 0
            iter_region = region
            possible_sites = [
                (i.start(1), i.end(1)) for i in pattern.finditer(iter_region)
            ]

            while possible_sites:
                bind_site = random.choice(possible_sites)
                bind_count += 1

                iter_region = self.set_mask(iter_region, *bind_site)

                possible_sites = [
                    (i.start(1), i.end(1)) for i in pattern.finditer(iter_region)
                ]

            density_dist[bind_count] += 1

        return {
            "min": min(density_dist.keys()),
            "mode": max(density_dist.iteritems(), key=itemgetter(1))[0],
            "max": max(density_dist.keys())
        }
