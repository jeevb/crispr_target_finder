import re

from Bio import SeqIO
from modules.candidate import Candidate
from modules.repeat_finder import RepeatFinder


class CrossChecker():

    def __init__(self, subjects, patterns, min_period, max_period, max_repeat_kmer):
        self.subjects = subjects
        self.patterns = patterns

        self.rf = RepeatFinder(
                min_period,
                max_period,
                max_repeat_kmer
            )

    def _matches(self, sequence, pattern):
        for match in re.compile(pattern).finditer(sequence):
            yield (
                match.start(1),
                match.end(1)
            )

    def find_extra_repeats(self, query):
        for pattern in self.patterns:
            for record in SeqIO.parse(open(self.subjects, "rU"), "fasta"):
                if record.name == query.source:
                    continue

                sequence = str(record.seq)

                candidate = Candidate()

                for start, end in self._matches(sequence, pattern["cross_checker"].format(word=query.word)):
                    candidate.add_loc((start, end))

                candidate.sort_locs()

                self.rf.find_repeats(candidate)

                if candidate.repeats:
                    query.add_extras(record.name, candidate.repeats)
