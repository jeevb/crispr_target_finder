class Repeat():

    def __init__(self, start):
        self.start = start
        self.end = start

        self.length = 1

        self.min_period = -1
        self.max_period = -1

        self.binding_potential = {"min": 0, "mode": 0, "max": 0}

    def extend(self, end, period):
        self.length += 1

        self.end = end + 1

        if self.min_period == -1 or period < self.min_period:
            self.min_period = period

        if self.max_period == -1 or period > self.max_period:
            self.max_period = period
