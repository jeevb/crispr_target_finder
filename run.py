from modules.candidate_finder import CandidateFinder
from settings import Settings

def main():
    cf = CandidateFinder(Settings())

    for candidate_idx, candidate in enumerate(cf.candidates()):
        for repeat_idx, repeat in enumerate(candidate.repeats):
            print "%s,%s,%i,%i,%i,%i,%i,%i,%i,%s,%s" % (
                "%i.%s.repeat_%i" % (
                    candidate_idx+1,
                    candidate.source,
                    repeat_idx+1
                ),
                "%s:%i" % (
                    candidate.source,
                    candidate.source_len
                ),
                repeat.start,
                repeat.end,
                repeat.binding_potential["min"],
                repeat.binding_potential["mode"],
                repeat.binding_potential["max"],
                repeat.min_period,
                repeat.max_period,
                str(candidate),
                ";".join(candidate.extras)
            )


if __name__ == "__main__":
    main()
