class Settings():

    # Path to the FASTA file containing the sequences within which
    # CRISPR targets will need to be identified
    subject_fasta = "/home/sba/noelle/ws240/c_elegans.PRJNA13758.WS240.genomic.fa"

    # Patterns for extraction of CRISPR targets
    # Typical CRISPR targets will have one of the following patterns:
    # (1) g(n)xngg
    # (2) ccn(n)xc
    patterns = [
        {
            "word_miner": "(?=((g.{{{word_len}}}).gg))",
            "cross_checker": "(?=({word}.gg))",
            "simulator": "(?=({word}[^x]gg[^x]{{{tail_len}}}))",
            "format": "{word}ngg"
        },
        {
            "word_miner": "(?=(cc.(.{{{word_len}}}c)))",
            "cross_checker": "(?=(cc.{word}))",
            "simulator": "(?=([^x]{{{tail_len}}}cc[^x]{word}))",
            "format": "ccn{word}"
        }
    ]

    # Bounds for sequence lengths
    min_word_length = 15
    max_word_length = 30

    # Sequences are characterized as forming repeats if the distance
    # between adjacent occurrences is between min_period and max_period
    # e.g. ATGC ... (N)x ... ATGC ... (N)x ... ATGC
    # min_period <= x <= max_period
    min_period = 30
    max_period = 100

    # Only consider candidates that form repeat regions consisting
    # of at least min_repeat_kmer subunits.
    min_repeat_kmer = 30

    # If a candidate forms a repeat region of more than
    # xref_max_repeat_kmer units with period length between
    # xref_min_period and xref_max_period in another subject, then
    # consider this a "non-specific" extra repeat.
    xref_min_period = 0
    xref_max_period = 100

    xref_max_repeat_kmer = 3

    # Settings for CRISPR binding simulator
    # The minimum number of nucleotides a bound CRISPR unit will span
    min_footprint = 30

    # Number of random trials
    trials = 1000
